<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', function () { return view('welcome'); });
Route::get('imprint', 'PageController@imprint');

Route::post('/link/create', 'ShortController@create');
Route::get('/link/delete-{id}', 'LinkCreateController@delete');

Route::get('/link/create', 'LinkCreateController@createView');
Route::post('/link/create-extend', 'LinkCreateController@create');
Route::post('/link/edit-{id}', 'EditLinkController@edit');
Route::post('/edit-user', 'UserController@edit');

Route::get('dashboard', 'DashboardController@index');
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('{hash}', 'ClickTrackingController@show');
