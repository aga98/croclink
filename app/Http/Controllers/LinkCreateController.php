<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use View;
use Mail;
use Session;
use Redirect;
use App\ShortLink;
use App\LinkAction;
use Illuminate\Http\Request;
use App\Mail\SendLinkDashboard;

class LinkCreateController extends Controller
{
    public function createView() {

        return view('shortlink.create');
    }

    public function delete($id) {
        $user = User::find(Auth::id());
        ShortLink::destroy($id);

        Session::flash('ShortLink', 'Link erfolgreich gelöscht!');

        return redirect::back();
    }

    public function create(Request $request) {

        $user = User::find(Auth::id());
        $hash = $this->GetHash();
        $name = $this->GetNameOfLink($hash, $request);
        
        if(strlen($request['own_hash']) > 1) {
            $hash = $request['own_hash'];

            if(ShortLink::where('hash', $hash)->count() != 0) {
                Session::flash('error', 'Linkendungen dürfen nicht doppelt erstellt werden!');

                return redirect('/dashboard');
            }
        }

        $Link = ShortLink::create([
            'name' => $name,
            'url' => $request['url'],
            'user_id' => $user->id,
            'hash' => $hash,
            'action' => $request['url']
        ]);

        $this->CreateActions($Link, $request);

        $this->NotifyUser($Link, $user);

        Session::flash('ShortLink', 'Link erfolgreich erstellt!');

        return redirect('/dashboard');
    }

    private function CreateActions($Link, $request) {
        if (!empty($request['facebookvisitpage'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpage'],
                'action' => 'facebookvisitpage',
            ]);
        }
        if (!empty($request['facebookvisitpage2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpage2'],
                'action' => 'facebookvisitpage',
            ]);
        }
        if (!empty($request['facebookvisitpage3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpage3'],
                'action' => 'facebookvisitpage',
            ]);
        }
        if (!empty($request['facebookvisitpost'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpost'],
                'action' => 'facebookvisitpost',
            ]);
        }
        if (!empty($request['facebookvisitpost2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpost2'],
                'action' => 'facebookvisitpost',
            ]);
        }
        if (!empty($request['facebookvisitpost3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['facebookvisitpost3'],
                'action' => 'facebookvisitpost',
            ]);
        }
        if (!empty($request['youtubeabocanal'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubeabocanal'],
                'action' => 'youtubeabocanal',
            ]);
        }
        if (!empty($request['youtubeabocanal2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubeabocanal2'],
                'action' => 'youtubeabocanal',
            ]);
        }
        if (!empty($request['youtubeabocanal3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubeabocanal3'],
                'action' => 'youtubeabocanal',
            ]);
        }
        if (!empty($request['youtubewatchvideo'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubewatchvideo'],
                'action' => 'youtubewatchvideo',
            ]);
        }
        if (!empty($request['youtubewatchvideo2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubewatchvideo2'],
                'action' => 'youtubewatchvideo',
            ]);
        }
        if (!empty($request['youtubewatchvideo3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['youtubewatchvideo3'],
                'action' => 'youtubewatchvideo',
            ]);
        }
        if (!empty($request['websitevisit'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['websitevisit'],
                'action' => 'websitevisit',
            ]);
        }
        if (!empty($request['websitevisit2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['websitevisit2'],
                'action' => 'websitevisit',
            ]);
        }
        if (!empty($request['websitevisit3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['websitevisit3'],
                'action' => 'websitevisit',
            ]);
        }
        if (!empty($request['instagramvisitpage'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpage'],
                'action' => 'instagramvisitpage',
            ]);
        }
        if (!empty($request['instagramvisitpage2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpage2'],
                'action' => 'instagramvisitpage',
            ]);
        }
        if (!empty($request['instagramvisitpage3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpage3'],
                'action' => 'instagramvisitpage',
            ]);
        }
        if (!empty($request['instagramvisitpost'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpost'],
                'action' => 'instagramvisitpost',
            ]);
        }
        if (!empty($request['instagramvisitpost2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpost2'],
                'action' => 'instagramvisitpost',
            ]);
        }
        if (!empty($request['instagramvisitpost3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['instagramvisitpost3'],
                'action' => 'instagramvisitpost',
            ]);
        }
        if (!empty($request['twittervisitpage'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpage'],
                'action' => 'twittervisitpage',
            ]);
        }
        if (!empty($request['twittervisitpage2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpage2'],
                'action' => 'twittervisitpage',
            ]);
        }
        if (!empty($request['twittervisitpage3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpage3'],
                'action' => 'twittervisitpage',
            ]);
        }
        if (!empty($request['twittervisitpost'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpost'],
                'action' => 'twittervisitpost',
            ]);
        }
        if (!empty($request['twittervisitpost2'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpost2'],
                'action' => 'twittervisitpost',
            ]);
        }
        if (!empty($request['twittervisitpost3'])) {
            LinkAction::create([
                'link_id' => $Link->id,
                'action_value' => $request['twittervisitpost3'],
                'action' => 'twittervisitpost',
            ]);
        }
    }

    private function NotifyUser($Link, $user) {

        if(!Auth::check() and !empty($request['email']))
        {
            Mail::send('emails.linkdashboard', ['item' => $Link], function ($message) use ($user, $Link)
            {
                $message->from('postmaster@mg.crocl.ink', 'CrocLink');
                $message->to($user['email']);
                $message->subject('Dein Link "'.$Link['name']." ist bereit!");
            });
        }
    }

    private function GetNameOfLink($hash, $request) {

        if (empty($request['name'])) {
            return 'http://crocl.ink/'.$hash;
        }
        else {
            return $request['name'];
        }
    }

    private function GetHash() {

        while ($hashexits = true) {
            return str_random(5);

            $counthash = ShortLink::where('hash', $hash)->count();

            if ($counthash == 0) {
                $hashexits = false;
                break;
            }
            else {
                $hashexits = false;
            }
        }
    }
}
