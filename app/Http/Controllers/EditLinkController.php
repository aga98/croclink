<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use View;
use Mail;
use Session;
use Redirect;
use App\ShortLink;
use App\LinkAction;
use Illuminate\Http\Request;
use App\Mail\SendLinkDashboard;

class EditLinkController extends Controller
{
    public function edit(Request $request, $id) {
        $link = ShortLink::findOrFail($id);

        $link->url = $request['url'];
        $link->name = $request['name'];
        $link->save();

        Session::flash('ShortLink', 'Link erfolgreich bearbeitet!');

        return redirect('/dashboard');
    }
}