<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use View;
use Mail;
use Session;
use Redirect;
use App\ShortLink;
use Illuminate\Http\Request;
use App\Mail\SendLinkDashboard;

class ShortController extends Controller
{
    public function createView() {

        return view('shortlink.create');
    }

    public function create (Request $request) {

        $user = $this->CheckUserID($request);
        $hash = $this->GetHash();
        $name = $this->GetNameOfLink($hash, $request);

        if(Auth::check()){
            $userId = $user->id;
        }
        else {
            $userId = 0;
        }

        $Link = ShortLink::create([
            'name' => $name,
            'email' => $request['email'],
            'url' => $request['url'],
            'user_id' => $userId,
            'hash' => $hash,
            'action' => $request['url']
        ]);

        $this->NotifyUser($Link, $user);

        Session::flash('ShortLink', 'http://crocl.ink/'.$hash);

        return Redirect::back();
    }

    private function NotifyUser($Link, $user) {

        if(!Auth::check() and !empty($request['email']))
        {
            Mail::send('emails.linkdashboard', ['item' => $Link], function ($message) use ($user, $Link)
            {
                $message->from('postmaster@mg.crocl.ink', 'CrocLink');
                $message->to($user['email']);
                $message->subject('Dein Link "'.$Link['name']." ist bereit!");
            });
        }
    }

    private function CheckUserID($request) {

        if(Auth::check()){
            return User::find(Auth::id());
        }

        else {
            $user = User::where('email', '=', $request['email'])->first();

            if (count($user) == 0 and !empty($request['email'])) {
                $user = User::create([
                    'name' => $request['email'],
                    'email' => $request['email'],
                    'password' => '',
                    'provider' => '',
                    'provider_id' => '',
                ]);
            }

            return $user;
        }
    }

    private function GetNameOfLink($hash, $request) {

        if (empty($request['name'])) {
            return 'http://crocl.ink/'.$hash;
        }
        else {
            return $request['name'];
        }
    }

    private function GetHash() {

        while ($hashexits = true) {
            return str_random(5);

            $counthash = ShortLink::where('hash', $hash)->count();

            if ($counthash == 0) {
                $hashexits = false;
                break;
            }
            else {
                $hashexits = false;
            }
        }
    }
}
