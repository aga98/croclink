<?php

namespace App\Http\Controllers;

use Auth;
use App\ClickTracking;
use Carbon\Carbon;
use App\LinkAction;
use View;
use Mail;
use Session;
use Redirect;
use App\ShortLink;
use Request;

class ClickTrackingController extends Controller
{
    public function show($hash)
    {
        $shortLink = ShortLink::where('hash', $hash)->first();

        $actions = $this->GetActions($shortLink);
        $this->TrackClick($shortLink);

        $ip = \Request::ip();

        if(count($actions) == 0) {
            if(substr( $shortLink->url, 0, 7 ) === "http://" or substr( $shortLink->url, 0, 8 ) === "https://") {
                return redirect($shortLink->url);
            }
            else {
                return redirect('http://'.$shortLink->url);
            }
        }

        if(substr( $shortLink->url, 0, 7 ) === "http://" or substr( $shortLink->url, 0, 8 ) === "https://") {
            $shortLink->url = $shortLink->url;
        }
        else {
            $shortLink->url = 'http://'.$shortLink->url;
        }

        return View::make('shortlink.landingpage', compact('shortLink'), compact('actions'));
    }

    private function GetActions($shortLink) {

        return LinkAction::where('link_id', '=', $shortLink->id)->get();
    }

    private function TrackClick($shortLink) {

        $userIP = \Request::ip();

        $date = Carbon::today();
        $date = $date->format('Y-m-d');

        $json  = file_get_contents("http://api.ipstack.com/".$userIP."?access_key=23e7d2cd2202f8ff66fcfde4f4a66633");
        $json  =  json_decode($json ,true);
        
        $Click = ClickTracking::where('ip_address', '=', $json['ip'])->where('shortlink_id', '=', $shortLink->id)->where('date', '=', $date)->first();
        $ClickCount = ClickTracking::where('ip_address', '=', $json['ip'])->where('shortlink_id', '=', $shortLink->id)->where('date', '=', $date)->count();

        if( $ClickCount == 0) {
            ClickTracking::create([
                'ip_address' => $json['ip'],
                'country' => $json['country_name'],
                'language' => $json['country_code'],
                'latitude' => $json['latitude'],
                'longitude' => $json['longitude'],
                'count' => '1',
                'user_id' => $shortLink->user_id,
                'shortlink_id' => $shortLink->id,
                'date' => $date,
            ]);
        }
        else {
            $Click->count++;
            $Click->save();
        }
    }
}
