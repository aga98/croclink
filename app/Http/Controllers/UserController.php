<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use View;
use Mail;
use Session;
use Redirect;
use App\ShortLink;
use App\LinkAction;
use Illuminate\Http\Request;
use App\Mail\SendLinkDashboard;

class UserController extends Controller
{
    public function edit(Request $input) {
        $user = User::find(Auth::id());

        $user->email = $input["email"];
        if ($input->has('password')) {
            $user->password = bcrypt($input['password']);
        }
        $user->save();

        Session::flash('ShortLink', 'Benutzer erfolgreich bearbeitet!');

        return redirect('/dashboard');
    }
}