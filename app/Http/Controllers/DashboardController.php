<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Carbon\Carbon;
use App\ClickTracking;
use Charts;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $links = $this->getLinks($request);

        $linkID = $request->input('link');

        if (!empty($linkID)) {
            $detailLink = DB::table('short_links')->where('user_id', $user->id)->where('id', $request->input('link'))->first();
        }
        else {
            $detailLink = DB::table('short_links')->where('user_id', $user->id)->first();
        }

        if($detailLink != null) {
            $detailLink->clickData = DB::table('click_trackings')->where('shortlink_id', $detailLink->id)->get();

            $detailLink->clicks = 0;

            foreach ($detailLink->clickData as $click) {
                $detailLink->clicks = $detailLink->clicks + $click->count;
            }

            foreach ($links as $link) {
                $clicks = DB::table('click_trackings')->where('shortlink_id', $link->id)->get();
                $link->clicks = 0;

                foreach ($clicks as $click) {
                    $link->clicks = $link->clicks + $click->count;
                }
            }

            $detailLink->chart = $this->getCharts($detailLink, $request);
            $detailLink = $this->getChartsCountry($detailLink);
        }

        else {
            return redirect('link/create');
        }

        return view('dashboard', compact('links'), compact('detailLink'), compact('chart'));
    }

    private function getChartsCountry($detailLink) {
        $countryName = array();
        $countryCount = array();

        foreach ($detailLink->clickData as $click) {
            if (in_array($click->country, $countryName)) {
                $key = array_search($click->country, $countryName);
                $countryCount[$key] = $countryCount[$key] + $click->count;
            }
            else {
                array_push($countryName,$click->country);
                array_push($countryCount,$click->count);
            }
        }

        $detailLink->countryName = $countryName;
        $detailLink->countryCount = $countryCount;

        return $detailLink;
    }

    private function getCharts($link, $request) {

        $clickData = $this->getChartsData($link, $request);
        $i = 0;

        if(isset($request['date'])) {
            $date = Carbon::parse($request['date']);
        }
        else {
            $date = Carbon::today()->subDay(8);
        }

        $dateArray=array("a"=>"red");
        while ($i <= 10) {
            $dateFormat = $date->format('d.m');
            array_push($dateArray,$dateFormat);
            $date = $date->addDay();
            $i++;
        }

        return Charts::multi('bar', 'material')
            ->title(false)
            ->elementLabel(false)
            ->legend(false)
            ->dimensions(0, 230)
            ->backgroundColor('#3aafa9')
            ->loader(true)->loaderColor('#fff')
            ->colors(['#fff'])
            ->labels([$dateArray['0'], $dateArray['1'], $dateArray['2'], $dateArray['3'], $dateArray['4'], $dateArray['5'], $dateArray['6'], $dateArray['7'], $dateArray['8']])
            ->dataset(false, $clickData);
    }

    private function getChartsData($link, $request) {

        $i = 0;
        $clickArray=array("a"=>"red");

        if(isset($request['date'])) {
            $date = Carbon::parse($request['date']);
        }
        else {
            $date = Carbon::today()->subDay(8);
        }

        while ($i <= 10) {
            $clicks = 0;
            $dateQuery = $date->format('Y-m-d');
            $clickData = DB::table('click_trackings')->where([
                ['shortlink_id', '=', $link->id],
                ['date', '=', $dateQuery],
            ])->get();

            foreach ($clickData as $item) {
                $clicks = $clicks + $item->count;
            }

            array_push($clickArray,$clicks);

            $date = $date->addDay();
            $i++;
        }

        return $clickArray;
    }

    private function getLinks($request) {
        $user = Auth::user();

        $links = DB::table('short_links')->where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

        foreach ($links as $link) {
            $clicks = DB::table('click_trackings')->where('shortlink_id', $link->id)->get();
            $link->clicks = 0;

            foreach ($clicks as $click) {
                $link->clicks = $link->clicks + $click->count;
            }
        }

        if($request->input('sortBy') == 'dateup') {
            return $links->sortByDesc('created_at');
        }
        else if($request->input('sortBy') == 'datedown'){
            return $links->sortBy('created_at');
        }
        else if($request->input('sortBy') == 'namedown'){
            return DB::table('short_links')->where('user_id', $user->id)->orderBy('name', 'asc')->get();
        }
        else if($request->input('sortBy') == 'nameup'){
            return DB::table('short_links')->where('user_id', $user->id)->orderBy('name', 'desc')->get();
        }
        else if($request->input('sortBy') == 'clickdown'){
            return $links->sortBy('clicks');
        }
        else if($request->input('sortBy') == 'clickup'){
            return $links->sortByDesc('clicks');
        }
        else {
            return $links;
        }
    }
}
