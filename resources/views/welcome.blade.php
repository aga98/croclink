@extends('layouts.app')

@section('content')
    @include('/frontpage/cover')
    @include('/frontpage/facts')
    @include('/frontpage/yourlinks')
@endsection
