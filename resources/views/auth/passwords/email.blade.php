@extends('layouts.login')
@section('content')
    <div class="login-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="col-md-8 col-md-offset-2">
                        <img src="{{asset('image/svg/logo.svg')}}" class="img-responsive login-logo">
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-4">
                    <div class="col-xs-12 text-center">
                        <h2>Passwort zurücksetzen!</h2>
                    </div>
                    @if (session('status'))
                        <div class="col-xs-12">
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif
                    <form class="login-form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-xs-12 control-label">E-Mail Address</label>

                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-primary col-xs-12 login-btn">
                                    Senden
                                </button>
                                <hr class="hr-small" />
                                <a class="btn btn-link" href="/login">
                                    <i class="fa fa-chevron-left" aria-hidden="true"></i> Zurück zum Login
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

