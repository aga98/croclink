@extends('layouts.login')
@section('content')
    <div class="login-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="col-md-8 col-md-offset-2">
                        <img src="{{asset('image/svg/logo.svg')}}" class="img-responsive login-logo">
                    </div>
                </div>

                <div class="col-md-4 col-md-offset-4">
                    <div class="col-xs-12 text-center">
                        <h2>Melde dich an!</h2>
                    </div>
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-xs-12 control-label">E-Mail Address</label>
                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email"
                                       value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-xs-12 control-label">Passwort</label>

                            <div class="col-xs-12">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        Angemeldet bleiben?
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12 text-center">
                                <button type="submit" class="btn btn-primary col-xs-12">
                                    Login
                                </button>
                                <br/>
                                <br/>
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Passwort vergessen?
                                </a>
                                <hr class="hr-small" />
                                <a class="btn btn-link" href="/register">
                                    Noch keinen Account? <br/> Registriere dich kostenlos!
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
