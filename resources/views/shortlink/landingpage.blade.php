@extends('layouts.app')

@section('CustomHeadTags')
<META NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW">
@endsection

@section('content')
    <div class="show-link-wrapper">
        <div class="container" style="position: relative;">

            <div class="visible-lg">
                <div style="position: absolute;top: 30px;left: -150px;width: 150px;height: 600px;">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Croclink -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-6461063866239848"
                         data-ad-slot="9221141568"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>

                <div style="position: absolute;top: 30px;right: -150px;width: 150px;height: 600px;">
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Croclink -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-6461063866239848"
                         data-ad-slot="9221141568"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>

            <div class="show-link-cover">
                <div>
                    <h1>{{$shortLink->name}}</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default action-panel">
                        @if(count($actions) != 0)
                            <div class="panel-heading"><i class="fa fa-check-square-o" aria-hidden="true"></i>
                                <span>@lang('linkdetail.tojob')</span>
                            </div>
                        @endif

                        <div class="panel-body text-center">
                            @foreach($actions as $action)
                                @if($action->action == 'facebookvisitpage')
                                    <a class="action-item action-facebook js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                            <span class="action-item-title">Facebook</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpage')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'facebookvisitpost')
                                    <a class="action-item action-facebook js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                            <span class="action-item-title">Facebook</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpost')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'youtubeabocanal')
                                    <a class="action-item action-youtube js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                            <span class="action-item-title">Youtube</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.abocanal')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'youtubewatchvideo')
                                    <a class="action-item action-youtube js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                            <span class="action-item-title">Youtube</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.watchvideo')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'twittervisitpage')
                                    <a class="action-item action-twitter js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                            <span class="action-item-title">Twitter</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpage')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'twittervisitpost')
                                    <a class="action-item action-twitter js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-twitter" aria-hidden="true"></i>
                                            <span class="action-item-title">Twitter</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpost')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'websitevisit')
                                    <a class="action-item action-website js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-globe" aria-hidden="true"></i>
                                            <span class="action-item-title">Website</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitwebsite')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'instagramvisitpost')
                                    <a class="action-item action-instagram js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                            <span class="action-item-title">Instagram</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpost')</span>
                                        </div>
                                    </a>
                                @elseif($action->action == 'instagramvisitpage')
                                    <a class="action-item action-instagram js-linkaction-visit" href="{{$action->action_value}}" target="_blank">
                                        <div class="action-item-inner">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                            <span class="action-item-title">Instagram</span>
                                            <div class="action-item-status unchecked">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </div>
                                            <span class="action-item-action">@lang('linkdetail.visitpage')</span>
                                        </div>
                                    </a>
                                @endif
                            @endforeach

                            <a class="btn btn-primary js-tolink-btn" href="" link="{{$shortLink->url}}" @if(count($actions) != 0) disabled="" @endif>
                                Weiter zur Website
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div style="margin-top: 30px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Croclink -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-6461063866239848"
                     data-ad-slot="9221141568"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
@endsection
