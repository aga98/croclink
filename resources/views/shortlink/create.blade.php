@extends('layouts.app')
@section('content')

    <div class="linkcreate-wrapper">
        <div class="container">
            <div class="linkcreate-header">
                <h1>Erstellen Sie einen Croclink</h1>
            </div>
            <form action="/link/create-extend" method="post">
                {{ csrf_field() }}

                <div class="row linkcreate-row">
                    <div class="col-md-2 text-center">
                        <span class="linkcreate-stepnumber">1</span>
                    </div>
                    <div class="col-md-3">
                        <span class="linkcreate-title">Link eintragen:</span>
                    </div>
                    <div class="col-md-7">
                        <input class="linkcreate-input" type="text" name="url" required="" placeholder="Bitte Link einfügen">
                    </div>
                </div>

                <div class="row linkcreate-row">
                    <div class="col-md-2 text-center">
                        <span class="linkcreate-stepnumber">2</span>
                    </div>
                    <div class="col-md-3">
                        <span class="linkcreate-title">Linktitel:</span>
                        <span class="linkcreate-desc">Im Dashboard sichtbar</span>
                    </div>
                    <div class="col-md-7">
                        <input class="linkcreate-input" type="text" name="name" required="" placeholder="Bitte Linktitel einfügen">
                    </div>
                </div>

                <div class="row linkcreate-row">
                    <div class="col-md-2 text-center">
                        <span class="linkcreate-stepnumber">3</span>
                    </div>
                    <div class="col-md-3">
                        <span class="linkcreate-title">Benutzerdefinierte Linkendung(optional)</span>
                    </div>
                    <div class="col-md-1">
                        <span style="padding-top: 30px; display: block;" class="linkcreate-desc">http://crocl.ink/</span>
                    </div>
                    <div class="col-md-4">
                        <input style="margin-left: 15px;" class="linkcreate-input" type="text" name="own_hash">
                    </div>
                </div>

                <div class="row linkcreate-row">
                    <div class="col-md-2 text-center">
                        <span class="linkcreate-stepnumber">4</span>
                    </div>
                    <div class="col-md-10">
                        <span class="linkcreate-title">Zusätzliche Funktionen <small>(optional)</small></span>
                        <span class="linkcreate-desc">Wird nichts ausgewählt wird der User direkt zum angegeben Link weitergeleitet.</span>
                    </div>
                    <div class="clearfix"></div>

                    @include('linkjobs.overview')

                </div>
                <hr />
                <button class="btn btn-primary btn-lg col-md-2 pull-right" type="submit"><i class="fa fa-link" aria-hidden="true"></i> CROC IT</button>
            </form>
            <div class="clearfix"></div>
            <div style="margin-top: 60px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Croclink -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-6461063866239848"
                     data-ad-slot="9221141568"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>

@endsection
