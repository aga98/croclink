
<div class="row">
    <div class="col-xs-12">
        <form action="/link/create" method="post">
            {{ csrf_field() }}
            <div class="cover-shortlink-wrapper">
                <input type="text" name="url"  class="cover-shortlink-input" required="" placeholder="@lang('frontpage.pastelink')">
                <button class="cover-shortlink-btn btn btn-primary" type="submit"><i class="fa fa-link" aria-hidden="true"></i> CROC IT</button>
            </div>
        </form>
    </div>
</div>

@if (Session::has('ShortLink'))
    <div class="cover-alert">{{ Session::get('ShortLink') }}</div>
@endif