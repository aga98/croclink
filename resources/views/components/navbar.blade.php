<nav class="navbar navbar-croclink navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="current-dev">BETA Version</div>
            <a href="" href="{{ url('/') }}" style="width: 37px;float: left;padding-right: 10px;padding-top: 13px;">
                <img src="{{asset('image/svg/crocanimal.svg')}}" class="img-responsive">
            </a>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{asset('image/svg/logo.svg')}}" class="img-responsive">
            </a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden"><a href="{{ route('register') }}">@lang('nav.help')</a></li>
                <li><a href="/imprint">@lang('nav.imprint')</a></li>
                @guest
                    <li><a style="padding-right: 0" href="{{ route('login') }}"><span class="btn btn-primary btn-nav"><i class="fa fa-sign-in" aria-hidden="true"></i> Login</span></a></li>
                    <li><a href="{{ route('register') }}"><span class="btn btn-primary btn-nav" ><i class="fa fa-sign-in" aria-hidden="true"></i> Register</span></a></li>
                @else
                    <li><a style="padding-right: 0" href="/dashboard"><span class="btn btn-primary btn-nav"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</span></a></li>
                    <li>
                        <a style="padding-right: 0">
                        <span class="btn btn-nav btn-primary" data-toggle="modal" data-target="#user">
                            <i class="fa fa-user-o" aria-hidden="true"></i> Benutzer
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                            <span class="btn btn-primary btn-nav"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</span>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
@guest

@else
<div class="modal fade" id="user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="post" action="/edit-user">
            {{ csrf_field() }}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Benutzer bearbeiten</h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <label for="usr">Email:</label>
                        <input type="text" name="email" class="form-control" value="{{ Auth::user()->email }}">
                    </div>
                    <div class="form-group">
                        <label for="usr">Passwort:</label>
                        <input type="password" name="password" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="usr">Passwort wiederholen:</label>
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endguest