<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Croclink</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        {!! Charts::styles() !!}
        <link rel="apple-touch-icon" sizes="180x180" href="/image/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/image/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/image/favicon/favicon-16x16.png">
        <link rel="manifest" href="/image/favicon/manifest.json">
        <link rel="mask-icon" href="/image/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113133968-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-113133968-1');
        </script>
        @yield('CustomHeadTags')
    </head>
    <body>
        @include('components.navbar')
        @yield('content')
        <script src="{{ asset('js/app.js') }}"></script>
        {!! Charts::scripts() !!}
        <div class="chart">
            @yield('script')
        </div>
    </body>
</html>
