@extends('layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="container">

            @include('/dashboard/link-detail')
            @include('/dashboard/toolbar')

            @if (Session::has('ShortLink'))
                <div class="alert alert-success">{{ Session::get('ShortLink') }}</div>
            @endif
            @if (Session::has('error'))
                <div class="alert alert-warning">{{ Session::get('error') }}</div>
            @endif

            <div class="tab-content">
                <div id="list" class="tab-pane fade in active">
                    @include('/dashboard/link-list')
                </div>
                <div id="panel" class="tab-pane fade">
                    <div class="row">
                        @include('/dashboard/link-panel')
                    </div>
                </div>
            </div>
            <div style="margin-top: 60px;">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Croclink -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-6461063866239848"
                     data-ad-slot="9221141568"
                     data-ad-format="auto"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
        </div>
    </div>
@endsection

@section('script')
    @if($detailLink != null)
        {!! $detailLink->chart->script() !!}
    @endif
@endsection