<div class="dashboard-list">
    <table class="table">
        @foreach($links as $link)
            <tr>
                <td><a href="/dashboard?link={{$link->id}}">{{$link->name}}</td>
                <td>http://crocl.ink/{{$link->hash}}</td>
                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $link->created_at)->format('d.m.Y H:i') }}</td>
                <td>{{$link->clicks}} Klicks</td>
                <td class="text-right">
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#edit{{$link->id}}">
                        <i class="fa fa-pencil" aria-hidden="true"></i> Bearbeiten
                    </button>
                    <a class="btn btn-primary btn-xs" href="/link/delete-{{$link->id}}"><i class="fa fa-trash-o" aria-hidden="true"></i> Löschen</a>
                </td>
            </tr>
        @endforeach
    </table>
</div>

@foreach($links as $link)
<div class="modal fade" id="edit{{$link->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form method="post" action="/link/edit-{{$link->id}}">
            {{ csrf_field() }}

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Bearbeiten</h4>
                </div>
                <div class="modal-body">

                        <div class="form-group">
                            <label for="usr">Name:</label>
                            <input type="text" name="name" class="form-control" value="{{$link->name}}">
                        </div>
                        <div class="form-group">
                            <label for="usr">Link:</label>
                            <input type="text" name="url" class="form-control" value="{{$link->url}}">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endforeach