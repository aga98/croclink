@foreach($links as $link)
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                {{$link->name}}
                <span class="pull-right">{{$link->created_at}}</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        http://crocl.ink/{{$link->hash}}<br/>
                        <small>{{$link->url}}</small>
                    </div>
                    <div class="col-md-6 text-right">
                        {{$link->clicks}} Klicks
                    </div>
                </div>

            </div>
        </div>
    </div>
@endforeach