<div class="dashboard-toolbar">
    <div class="row">
        <div class="col-md-3 hidden">
            <input type="text" class="form-control">
        </div>
        <div class="col-md-6">
            <a href="/link/create" class="btn btn-primary"><i class="fa fa-plus" aria-hidden="true"></i> Croclink erstellen</a>
            <a href="" class="btn btn-primary"><i class="fa fa-refresh" aria-hidden="true"></i> Aktualisieren</a>
        </div>

        <div class="col-md-6 text-right">
            <div class="dropdown" style="display: inline-block; margin-right: 20px;">
                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-sort" aria-hidden="true"></i> Sortieren nach
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="?sortBy=datedown">Datum aufsteigend</a></li>
                    <li><a href="?sortBy=dateup">Datum absteigend</a></li>
                    <li><a href="?sortBy=namedown">Name aufsteigend</a></li>
                    <li><a href="?sortBy=nameup">Name absteigend</a></li>
                    <li><a href="?sortBy=clickdown">Klick aufsteigend</a></li>
                    <li><a href="?sortBy=clickup">Klick absteigend</a></li>
                </ul>
            </div>
            <a class="btn btn-primary"  data-toggle="tab" href="#panel"><i class="fa fa-columns" aria-hidden="true"></i></a>
            <a class="btn btn-primary" data-toggle="tab" href="#list"><i class="fa fa-list" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
