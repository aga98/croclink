@if($detailLink != null)
    <div class="dashboard-detail">
        <span class="dashboard-detail-created">Erstellt: {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $detailLink->created_at)->format('d.m.Y') }}</span>
        <span class="dashboard-detail-hash">crocl.ink/{{ $detailLink->hash }}</span>
        <input class="dashboard-detail-url" value="{{ $detailLink->url }}"/>
        <span class="dashboard-detail-name">{{ $detailLink->name }}</span>
        <span class="dashboard-detail-clicks"><i class="fa fa-signal" aria-hidden="true"></i>{{ $detailLink->clicks }}</span>
        <div class="dashboard-detail-country">
            @foreach($detailLink->countryName as $k => $v)
                @if ($k < 3)
                    {{$detailLink->countryCount[$k]}} {{ $v }}<br/>
                @endif
            @endforeach
        </div>
        <div class="pull-right" style="width: 723px;     top: 20px; position: relative;">
            {!! $detailLink->chart->html() !!}
            <div class="dashboard-detail-charts-overlay"></div>
        </div>
        <a href="?date={{ \Carbon\Carbon::parse(app('request')->input('date'))->subDay(7)->format('d.m.Y') }}" class="dashboard-detail-week-ago">Woche zurück</a>
        <a href="?date={{ \Carbon\Carbon::parse(app('request')->input('date'))->addDay(7)->format('d.m.Y') }}" class="dashboard-detail-week-before">Woche nachvor</a>
    </div>
@endif