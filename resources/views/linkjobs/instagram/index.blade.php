<div class="col-md-12">
    <div class="collapse" id="instagramvisitpage">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.instagrampage')</label>
                <input type="url" name="instagramvisitpage" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#instagramvisitpage2" aria-expanded="false" aria-controls="instagramvisitpage2">+ @lang('visit.another') @lang('visit.instagrampage')</span>

                <div class="collapse" id="instagramvisitpage2">
                    <hr/>
                    <label class="control-label">@lang('visit.instagrampage')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#instagramvisitpage2" aria-expanded="false" aria-controls="instagramvisitpage2">@lang('visit.remove')</label>
                    <input type="url" name="instagramvisitpage2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#instagramvisitpage3" aria-expanded="false" aria-controls="instagramvisitpage3">+ @lang('visit.another') @lang('visit.instagrampage')</span>

                    <div class="collapse" id="instagramvisitpage3">
                        <hr/>
                        <label class="control-label">@lang('visit.instagrampage')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#instagramvisitpage3" aria-expanded="false" aria-controls="instagramvisitpage2">@lang('visit.remove')</label>
                        <input type="url" name="instagramvisitpage3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="collapse" id="instagramvisitpost">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.instagrampost')</label>
                <input type="url" name="instagramvisitpost" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#instagramvisitpost2" aria-expanded="false" aria-controls="instagramvisitpost2">+ @lang('visit.another') @lang('visit.instagrampost')</span>

                <div class="collapse" id="instagramvisitpost2">
                    <hr/>
                    <label class="control-label">@lang('visit.instagrampost')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#instagramvisitpost2" aria-expanded="false" aria-controls="instagramvisitpost2">@lang('visit.remove')</label>
                    <input type="url" name="instagramvisitpost2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#instagramvisitpost3" aria-expanded="false" aria-controls="instagramvisitpost3">+ @lang('visit.another') @lang('visit.instagrampost')</span>

                    <div class="collapse" id="instagramvisitpost3">
                        <hr/>
                        <label class="control-label">@lang('visit.instagrampost')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#instagramvisitpost3" aria-expanded="false" aria-controls="instagramvisitpost2">@lang('visit.remove')</label>
                        <input type="url" name="instagramvisitpost3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>