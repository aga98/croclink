<div class="col-md-12">
    <div class="collapse" id="facebookvisitpage">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.facebookpage')</label>
                <input type="url" name="facebookvisitpage" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#facebookvisitpage2" aria-expanded="false" aria-controls="facebookvisitpage2">+ @lang('visit.another') @lang('visit.facebookpage')</span>

                <div class="collapse" id="facebookvisitpage2">
                    <hr/>
                    <label class="control-label">@lang('visit.facebookpage')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#facebookvisitpage2" aria-expanded="false" aria-controls="facebookvisitpage2">@lang('visit.remove')</label>
                    <input type="url" name="facebookvisitpage2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#facebookvisitpage3" aria-expanded="false" aria-controls="facebookvisitpage3">+ @lang('visit.another') @lang('visit.facebookpage')</span>

                    <div class="collapse" id="facebookvisitpage3">
                        <hr/>
                        <label class="control-label">@lang('visit.facebookpage')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#facebookvisitpage3" aria-expanded="false" aria-controls="facebookvisitpage2">@lang('visit.remove')</label>
                        <input type="url" name="facebookvisitpage3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="collapse" id="facebookvisitpost">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.facebookpost')</label>
                <input type="url" name="facebookvisitpost" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#facebookvisitpost2" aria-expanded="false" aria-controls="facebookvisitpost2">+ @lang('visit.another') @lang('visit.facebookpost')</span>

                <div class="collapse" id="facebookvisitpost2">
                    <hr/>
                    <label class="control-label">@lang('visit.facebookpost')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#facebookvisitpost2" aria-expanded="false" aria-controls="facebookvisitpost2">@lang('visit.remove')</label>
                    <input type="url" name="facebookvisitpost2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#facebookvisitpost3" aria-expanded="false" aria-controls="facebookvisitpost3">+ @lang('visit.another') @lang('visit.facebookpost')</span>

                    <div class="collapse" id="facebookvisitpost3">
                        <hr/>
                        <label class="control-label">@lang('visit.facebookpost')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#facebookvisitpost3" aria-expanded="false" aria-controls="facebookvisitpost2">@lang('visit.remove')</label>
                        <input type="url" name="facebookvisitpost3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>