<div class="col-md-12">
    <div class="collapse" id="youtubewatchvideo">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.youtubevideo')</label>
                <input type="url" name="youtubewatchvideo" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#youtubewatchvideo2" aria-expanded="false" aria-controls="youtubewatchvideo3">+ @lang('visit.another') @lang('visit.youtubevideo')</span>

                <div class="collapse" id="youtubewatchvideo2">
                    <hr/>
                    <label class="control-label">@lang('visit.youtubevideo')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#youtubewatchvideo2" aria-expanded="false" aria-controls="youtubewatchvideo2">@lang('visit.remove')</label>
                    <input type="url" name="youtubewatchvideo2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#youtubewatchvideo3" aria-expanded="false" aria-controls="facebookvisitpage3">+ @lang('visit.another') @lang('visit.youtubevideo')</span>

                    <div class="collapse" id="youtubewatchvideo3">
                        <hr/>
                        <label class="control-label">@lang('visit.youtubevideo')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#youtubewatchvideo3" aria-expanded="false" aria-controls="youtubewatchvideo2">@lang('visit.remove')</label>
                        <input type="url" name="youtubewatchvideo3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="collapse" id="youtubeabocanal">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.youtubecanal')</label>
                <input type="url" name="youtubeabocanal" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#youtubeabocanal2" aria-expanded="false" aria-controls="youtubeabocanal3">+ @lang('visit.another') @lang('visit.youtubecanal')</span>

                <div class="collapse" id="youtubeabocanal2">
                    <hr/>
                    <label class="control-label">@lang('visit.youtubecanal')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#youtubeabocanal2" aria-expanded="false" aria-controls="youtubeabocanal2">@lang('visit.remove')</label>
                    <input type="url" name="youtubeabocanal2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#youtubeabocanal3" aria-expanded="false" aria-controls="facebookvisitpage3">+ @lang('visit.another') @lang('visit.youtubecanal')</span>

                    <div class="collapse" id="youtubeabocanal3">
                        <hr/>
                        <label class="control-label">@lang('visit.youtubecanal')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#youtubeabocanal3" aria-expanded="false" aria-controls="youtubeabocanal2">@lang('visit.remove')</label>
                        <input type="url" name="youtubeabocanal3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
