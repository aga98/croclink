<div class="col-md-2"></div>
<div class="col-md-10">
    <br/>
    <div class="row">
        <div class="col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-youtube dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    Youtube
                </button>
                <ul class="dropdown-menu">
                    <li><a role="button" data-toggle="collapse" href="#youtubeabocanal" aria-expanded="false" aria-controls="youtubeabocanal">@lang('visit.canal')</a></li>
                    <li><a role="button" data-toggle="collapse" href="#youtubewatchvideo" aria-expanded="false" aria-controls="youtubewatchvideo">@lang('visit.video')</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-2 hidden">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-twitch dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-twitch" aria-hidden="true"></i>
                    Twitch
                </button>
                <ul class="dropdown-menu">
                    <li><a role="button" data-toggle="collapse" href="#facebookvisitpage" aria-expanded="false" aria-controls="facebookvisitpage">@lang('visit.page')</a></li>
                    <li><a role="button" data-toggle="collapse" href="#facebookvisitpost" aria-expanded="false" aria-controls="facebookvisitpost">@lang('visit.post')</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-twitter dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                    Twitter
                </button>
                <ul class="dropdown-menu">
                    <li><a role="button" data-toggle="collapse" href="#twittervisitpage" aria-expanded="false" aria-controls="twittervisitpage">@lang('visit.page')</a></li>
                    <li><a role="button" data-toggle="collapse" href="#twittervisitpost" aria-expanded="false" aria-controls="twittervisitpost">@lang('visit.post')</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-facebook dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                    Facebook
                </button>
                <ul class="dropdown-menu">
                    <li><a role="button" data-toggle="collapse" href="#facebookvisitpage" aria-expanded="false" aria-controls="facebookvisitpage">@lang('visit.page')</a></li>
                    <li><a role="button" data-toggle="collapse" href="#facebookvisitpost" aria-expanded="false" aria-controls="facebookvisitpost">@lang('visit.post')</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-instagram dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                    Instagram
                </button>
                <ul class="dropdown-menu">
                    <li><a role="button" data-toggle="collapse" href="#instagramvisitpage" aria-expanded="false" aria-controls="instagramvisitpage">@lang('visit.page')</a></li>
                    <li><a role="button" data-toggle="collapse" href="#instagramvisitpost" aria-expanded="false" aria-controls="instagramvisitpost">@lang('visit.post')</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-2">
            <div class="btn-group">
                <button type="button" class="btn btn-default linkcreate-btn linkcreate-website" data-toggle="collapse" href="#websitevisit" aria-expanded="false" aria-controls="websitevisit">
                    <i class="fa fa-link" aria-hidden="true"></i>
                    Website
                </button>
            </div>
        </div>
    </div>

    <div class="row">
        <hr />
    </div>

    <div class="row">
        @include('linkjobs.website.index')
        @include('linkjobs.twitter.index')
        @include('linkjobs.instagram.index')
        @include('linkjobs.facebook.index')
        @include('linkjobs.youtube.index')
    </div>
</div>
