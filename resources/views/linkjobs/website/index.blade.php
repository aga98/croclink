<div class="col-md-12">
    <div class="collapse" id="websitevisit">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.website')</label>
                <input type="url" name="websitevisit" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#websitevisit2" aria-expanded="false" aria-controls="websitevisit2">+ @lang('visit.another') @lang('visit.website')</span>

                <div class="collapse" id="websitevisit2">
                    <hr/>
                    <label class="control-label">@lang('visit.website')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#websitevisit2" aria-expanded="false" aria-controls="websitevisit2">@lang('visit.remove')</label>
                    <input type="url" name="websitevisit2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#websitevisit3" aria-expanded="false" aria-controls="websitevisit3">+ @lang('visit.another') @lang('visit.website')</span>

                    <div class="collapse" id="websitevisit3">
                        <hr/>
                        <label class="control-label">@lang('visit.website')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#websitevisit3" aria-expanded="false" aria-controls="websitevisit2">@lang('visit.remove')</label>
                        <input type="url" name="websitevisit3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>