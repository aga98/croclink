<div class="col-md-12">
    <div class="collapse" id="twittervisitpage">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.twitterpage')</label>
                <input type="url" name="twittervisitpage" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#twittervisitpage2" aria-expanded="false" aria-controls="twittervisitpage2">+ weitere @lang('visit.twitterpage')</span>

                <div class="collapse" id="twittervisitpage2">
                    <hr/>
                    <label class="control-label">@lang('visit.twitterpage')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#twittervisitpage2" aria-expanded="false" aria-controls="twittervisitpage2">@lang('visit.remove')</label>
                    <input type="url" name="twittervisitpage2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#twittervisitpage3" aria-expanded="false" aria-controls="twittervisitpage3">+ weitere @lang('visit.twitterpage')</span>

                    <div class="collapse" id="twittervisitpage3">
                        <hr/>
                        <label class="control-label">@lang('visit.twitterpage')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#twittervisitpage3" aria-expanded="false" aria-controls="twittervisitpage2">@lang('visit.remove')</label>
                        <input type="url" name="twittervisitpage3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="collapse" id="twittervisitpost">
        <div class="panel panel-default">
            <div class="panel-body">
                <label class="control-label">@lang('visit.twitterpost')</label>
                <input type="url" name="twittervisitpost" class="form-control" placeholder="@lang('visit.pasteurl')">
                <span role="button" data-toggle="collapse" href="#twittervisitpost2" aria-expanded="false" aria-controls="twittervisitpost2">+ weitere @lang('visit.twitterpost')</span>

                <div class="collapse" id="twittervisitpost2">
                    <hr/>
                    <label class="control-label">@lang('visit.twitterpost')</label>
                    <label class="control-label pull-right" role="button" data-toggle="collapse" href="#twittervisitpost2" aria-expanded="false" aria-controls="twittervisitpost2">@lang('visit.remove')</label>
                    <input type="url" name="twittervisitpost2" class="form-control" placeholder="@lang('visit.pasteurl')">
                    <span role="button" data-toggle="collapse" href="#twittervisitpost3" aria-expanded="false" aria-controls="twittervisitpost3">+ weitere @lang('visit.twitterpost')</span>

                    <div class="collapse" id="twittervisitpost3">
                        <hr/>
                        <label class="control-label">@lang('visit.twitterpost')</label>
                        <label class="control-label pull-right" role="button" data-toggle="collapse" href="#twittervisitpost3" aria-expanded="false" aria-controls="twittervisitpost2">@lang('visit.remove')</label>
                        <input type="url" name="twittervisitpost3" class="form-control" placeholder="@lang('visit.pasteurl')">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>