@extends('layouts.app')

@section('content')
    <div class="show-link-wrapper">
        <div class="container">
            <div class="show-link-cover">
                <div>
                    <h1>Impressum</h1>
                </div>
            </div>

            <h2>Impressum</h2>
            <p>Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und
                Offenlegungspflicht laut §25 Mediengesetz</p>
            <p>Croclink<br>
                Dominik&nbsp;Zwerger<br>
                Stuttgarterstraße 18<br>
                6330&nbsp;Kufstein</p>
                <strong>E-Mail:</strong> <a href="mailto:info@zwerger.cc">info@zwerger.cc</a></p>
            <p>Angaben zur Online-Streitbeilegung: Verbraucher haben die Möglichkeit, Beschwerden an die
                OnlineStreitbeilegungsplattform der EU zu richten: <a
                        href="https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.show&amp;lng=DE"
                        target="_blank" rel="noopener noreferrer nofollow"
                        class="external">http://ec.europa.eu/odr</a><span>. Sie können allfällige Beschwerde auch an die oben angegebene E-Mail-Adresse richten.</span>
            </p>
            <h2>Haftungsausschluss</h2>
            <p>Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese
                Webseite&nbsp;verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden
                Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des
                Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.</p>
            <h3>Haftung für Inhalte dieser Webseite</h3>
            <p>Die Inhalte unserer Seiten wurden mit größter Sorgfalt erstellt. Für die Richtigkeit, Vollständigkeit und
                Aktualität der Inhalte können wir jedoch keine Gewähr übernehmen. Als Diensteanbieter sind wir für
                eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Wir sind jedoch nicht
                verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu
                forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung
                der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine
                diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung
                möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend
                entfernen.</p>
            <h3>Haftung für Links auf Webseiten Dritter</h3>
            <p>Unser Angebot enthält Links zu externen Websites. Auf den Inhalt dieser externen Webseiten haben wir
                keinerlei Einfluss. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die
                Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich.
                Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft.
                Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche
                Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht
                zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
            <h3>Urheberrecht</h3>
            <p>Die Betreiber dieser Webseite&nbsp;sind bemüht, stets die Urheberrechte anderer zu beachten bzw. auf
                selbst erstellte sowie lizenzfreie Werke zurückzugreifen. Die durch die Seitenbetreiber erstellten
                Inhalte und Werke auf dieser Webseite unterliegen dem Urheberrecht. Beiträge Dritter sind als solche
                gekennzeichnet. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der
                Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers.
                Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.</p>
            <h2>Datenschutzerklärung</h2>
            <p>Wir legen großen Wert auf den Schutz Ihrer Daten. Um Sie in vollem Umfang über die Verwendung
                personenbezogener Daten zu informieren, bitten wir Sie die folgenden Datenschutzhinweise zur Kenntnis zu
                nehmen.</p>
            <h3>Persönliche Daten</h3>
            <p>Persönliche Daten, die Sie auf dieser&nbsp;Website elektronisch übermitteln, wie zum Beispiel Name,
                E-Mail-Adresse, Adresse&nbsp;oder andere persönlichen Angaben, werden von uns nur zum jeweils
                angegebenen Zweck verwendet, sicher verwahrt und nicht an Dritte weitergegeben. Der Provider erhebt und
                speichert automatisch Informationen am Webserver wie verwendeter Browser, Betriebssystem, Verweisseite,
                IP-Adresse, Uhrzeit des Zugriffs usw. Diese Daten können ohne Prüfung weiterer Datenquellen keinen
                bestimmten Personen zugeordnet werden und wir werten diese Daten auch nicht weiter aus solange keine
                rechtswidrige Nutzung unserer Webseite vorliegt.</p>
            <h3>Formulardaten und Kommentare</h3>
            <p>Wenn Webseitenbesucher Kommentare oder Formulareinträge hinterlassen, werden die eingegebenen Daten und
                ihre IP-Adressen gespeichert. Das erfolgt zur Sicherheit, falls jemand widerrechtliche Inhalte verfasst
                (Beleidigungen, links- oder rechtsextreme Propaganda, Hasspostings usw.). In diesem Fall sind wir&nbsp;an
                der Identität des Verfassers interessiert.</p>
            <h3>Cookies</h3>
            <p>Cookies sind kleine Dateien, die es dieser Webseite ermöglichen auf dem Computer des Besuchers
                spezifische, auf den Nutzer bezogene Informationen zu speichern, während unsere Website besucht wird.
                Cookies helfen uns dabei, die Nutzungshäufigkeit und die Anzahl der Nutzer unserer Internetseiten zu
                ermitteln, sowie unsere Angebote für Sie komfortabel und effizient zu gestalten. Wir verwenden
                einerseits Session-Cookies, die ausschließlich für die Dauer Ihrer Nutzung unserer Website
                zwischengespeichert werden und zum anderen permanente Cookies, um Informationen über Besucher
                festzuhalten, die wiederholt auf unsere Website zugreifen. Der Zweck des Einsatzes dieser Cookies
                besteht darin, eine optimale Benutzerführung anbieten zu können sowie Besucher wiederzuerkennen und bei
                wiederholter Nutzung eine möglichst attraktive Website und interessante Inhalte präsentieren zu können.
                Der Inhalt eines permanenten Cookies beschränkt sich auf eine Identifikationsnummer. Name, IP-Adresse
                usw. werden nicht gespeichert. Eine Einzelprofilbildung über Ihr Nutzungsverhalten findet nicht statt.
                Eine Nutzung unserer Angebote ist auch ohne Cookies möglich. Sie können in Ihrem Browser das Speichern
                von Cookies deaktivieren, auf bestimmte Webseiten beschränken oder Ihren Webbrowser (Chrome, IE,
                Firefox,…) so einstellen, dass er sie benachrichtigt, sobald ein Cookie gesendet wird. Sie können
                Cookies auch jederzeit von der Festplatte ihres PC löschen. Bitte beachten Sie aber, dass Sie in diesem
                Fall mit einer eingeschränkten Darstellung der Seite und mit einer eingeschränkten Benutzerführung
                rechnen müssen.</p>
            <h3>Datenschutzerklärung für die Nutzung von Google Analytics</h3>
            <p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. („Google“). Google
                Analytics verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert werden und die eine
                Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen
                über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen
                und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite, wird Ihre
                IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen
                Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen
                wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag
                des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website
                auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der
                Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu
                erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit
                anderen Daten von Google zusammengeführt. Sie können die Speicherung der Cookies durch eine
                entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie
                in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen
                können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der
                Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch
                Google verhindern, indem sie das unter dem folgenden Link (<a
                        href="http://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" class="external"
                        rel="nofollow">http://tools.google.com/dlpage/gaoptout?hl=de</a>) verfügbare Browser-Plugin
                herunterladen und installieren. </p>
            <p>Nähere Informationen zu Nutzungsbedingungen und Datenschutz finden Sie unter <a
                        href="http://www.google.com/analytics/terms/de.html" target="_blank" class="external"
                        rel="nofollow">http://www.google.com/analytics/terms/de.html</a> bzw. unter <a
                        href="https://support.google.com/analytics/answer/6004245?hl=de" target="_blank"
                        class="external" rel="nofollow">https://support.google.com/analytics/answer/6004245?hl=de</a>.
                Wir weisen Sie darauf hin, dass auf dieser Webseite Google Analytics um den Code „gat._anonymizeIp();“
                erweitert wurde, um eine anonymisierte Erfassung von IP-Adressen (sog. IP-Masking) zu gewährleisten.</p>
            <h3>Datenschutzerklärung für die Nutzung von&nbsp;Facebook</h3>
            <p>Auf unseren Seiten sind Plugins des sozialen Netzwerks Facebook, 1601 South California Avenue, Palo Alto,
                CA 94304, USA integriert. Die Facebook-Plugins erkennen Sie an dem Facebook-Logo oder dem „Like-Button“
                („Gefällt mir“) auf unserer Seite. Eine Übersicht über die Facebook-Plugins finden Sie hier: <a
                        href="https://developers.facebook.com/docs/plugins/" target="_blank" class="external"
                        rel="nofollow">https://developers.facebook.com/docs/plugins/</a>.</p>
            <p>Wenn Sie unsere Seiten besuchen, wird über das Plugin eine direkte Verbindung zwischen Ihrem Browser und
                dem Facebook-Server hergestellt. Facebook erhält dadurch die Information, dass Sie mit Ihrer IP-Adresse
                unsere Seite besucht haben. Wenn Sie den Facebook „Like-Button“ anklicken während Sie in Ihrem
                Facebook-Account eingeloggt sind, können Sie die Inhalte unserer Seiten auf Ihrem Facebook-Profil
                verlinken. Dadurch kann Facebook den Besuch unserer Seiten Ihrem Benutzerkonto zuordnen. Wir weisen
                darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom Inhalt der übermittelten Daten sowie
                deren Nutzung durch Facebook erhalten. Weitere Informationen hierzu finden Sie in der
                Datenschutzerklärung von Facebook unter <a href="https://www.facebook.com/policy.php" target="_blank"
                                                           class="external" rel="nofollow">https://www.facebook.com/policy.php</a>&nbsp;Wenn
                Sie nicht wünschen, dass Facebook den Besuch unserer Seiten Ihrem Facebook- Nutzerkonto zuordnen&nbsp;kann,
                loggen Sie sich bitte aus Ihrem Facebook-Benutzerkonto aus.</p>
            <h3>Datenschutzerklärung für die Nutzung von&nbsp;Google+</h3>
            <p>Mithilfe der Google +1-Schaltfläche können Sie Informationen weltweit veröffentlichen. über die Google
                +1-Schaltfläche erhalten Sie und andere Nutzer personalisierte Inhalte von Google und unseren Partnern.
                Google speichert sowohl die Information, dass Sie für einen Inhalt +1 gegeben haben, als auch
                Informationen über die Seite, die Sie beim Klicken auf +1 angesehen haben. Ihre +1 können als Hinweise
                zusammen mit Ihrem Profilnamen und Ihrem Foto in Google-Diensten, wie etwa in Suchergebnissen oder in
                Ihrem Google-Profil, oder an anderen Stellen auf Websites und Anzeigen im Internet eingeblendet
                werden.</p>
            <p>Google zeichnet Informationen über Ihre +1-Aktivitäten auf, um die Google-Dienste für Sie und andere zu
                verbessern. Um die Google +1- Schaltfläche verwenden zu können, benötigen Sie ein weltweit sichtbares,
                öffentliches Google-Profil, das zumindest den für das Profil gewählten Namen enthalten muss. Dieser Name
                wird in allen Google- Diensten verwendet. In manchen Fällen kann dieser Name auch einen anderen Namen
                ersetzen, den Sie beim Teilen von Inhalten über Ihr Google-Konto verwendet haben. Die Identität Ihres
                Google-Profils kann Nutzern angezeigt werden, die Ihre E-Mail-Adresse kennen oder über andere
                identifizierende Informationen von Ihnen verfügen.<br>
                Neben den oben erläuterten Verwendungszwecken werden die von Ihnen bereitgestellten Informationen gemäß
                den geltenden Google-Datenschutzbestimmungen genutzt. Google veröffentlicht möglicherweise
                zusammengefasste Statistiken über die +1-Aktivitäten der Nutzer bzw. gibt diese an Nutzer und Partner
                weiter, wie etwa Publisher, Inserenten oder verbundene Websites.</p>
            <h3>Datenschutzerklärung für die Nutzung von&nbsp;Twitter</h3>
            <p>Auf unserer Webseite sind Funktionen des Dienstes Twitter eingebunden. Diese Funktionen werden durch die
                Twitter Inc., Twitter, Inc. 1355 Market St, Suite 900, San Francisco, CA 94103, USA angeboten. Durch das
                Benutzen von Twitter und den Funktionen der Twitter-Buttons, werden die von Ihnen besuchten Webseiten
                mit Ihrem Twitter-Account verknüpft und anderen Nutzern bekannt gegeben. Dabei werden auch Daten an
                Twitter übertragen. Wir weisen darauf hin, dass wir als Anbieter der Webseite keine Kenntnis vom Inhalt
                der übermittelten Daten sowie deren Nutzung durch Twitter erhalten. Weitere Informationen hierzu finden
                Sie in der Datenschutzrichtline von Twitter unter <a href="https://twitter.com/privacy?lang=de"
                                                                     target="_blank" class="external" rel="nofollow">https://twitter.com/privacy?lang=de</a>.&nbsp;Ihre
                Datenschutzeinstellungen bei Twitter können Sie in den Konto- Einstellungen unter&nbsp;<a
                        href="https://twitter.com/settings/account" target="_blank" class="external" rel="nofollow">https://twitter.com/settings/account</a>
                ändern.</p>
            <h3>Datenschutzerklärung für die Nutzung von YouTube</h3>
            <p>Auf unserer Webseite sind Funktionen des Dienstes YouTube implementiert. Diese Funktionen werden durch
                die YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA angeboten. Die eingebundenen Videos legen
                bei dem Aufrufen der Webseite Cookies auf den Computern der User ab. Wer das Setzen von Cookies für das
                Google-Werbeprogramm deaktiviert hat, wird auch beim Aufrufen von YouTube-Videos mit keinen solchen
                Cookies rechnen müssen. YouTube legt aber auch in anderen Cookies nicht-personenbezogene
                Nutzungsinformationen ab. Möchten Sie dies verhindern, müssen Sie das im Browser blockieren.</p>
            <h3>Datenschutzerklärung für die Nutzung von Instagram</h3>
            <p>Auf unserer Webseite sind Funktionen des Dienstes Instagram eingebunden. Diese Funktionen werden
                angeboten durch die Instagram Inc., 1601 Willow Road, Menlo Park, CA, 94025, USA integriert. Wenn Sie in
                Ihren Instagram-Account eingeloggt sind, können Sie durch Anklicken des Instagram-Buttons die Inhalte
                unserer Seiten mit Ihrem Instagram-Profil verlinken. Dadurch kann Instagram den Besuch unserer Seiten
                Ihrem Benutzerkonto zuordnen. Wir weisen darauf hin, dass wir als Anbieter der Seiten keine Kenntnis vom
                Inhalt der übermittelten Daten sowie deren Nutzung durch Instagram erhalten.<br>
                Weitere Informationen hierzu finden Sie in der Datenschutzerklärung von Instagram auf&nbsp;<a
                        href="https://www.instagram.com/about/legal/privacy/" target="_blank" class="external"
                        rel="nofollow">https://www.instagram.com/about/legal/privacy/</a>.</p>
            <h3>Datenschutzerklärung für Google Adsense</h3>
            <p>Diese Website benutzt Google AdSense, einen Dienst zum Einbinden von Werbeanzeigen der Google Inc.
                („Google“). Google AdSense verwendet sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert
                werden und die eine Analyse der Benutzung der Website ermöglicht. Google AdSense verwendet auch so
                genannte Web Beacons (unsichtbare Grafiken). Durch diese Web Beacons können Informationen wie der
                Besucherverkehr auf diesen Seiten ausgewertet werden.</p>
            <p>Die durch Cookies und Web Beacons erzeugten Informationen über die Benutzung dieser Website
                (einschließlich Ihrer IP-Adresse) und Auslieferung von Werbeformaten werden an einen Server von Google
                in den USA übertragen und dort gespeichert. Diese Informationen können von Google an Vertragspartner von
                Google weiter gegeben werden. Google wird Ihre IP-Adresse jedoch nicht mit anderen von Ihnen
                gespeicherten Daten zusammenführen.</p>
            <p>Sie können die Installation der Cookies durch eine entsprechende Einstellung Ihrer Browser Software
                verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche
                Funktionen dieser Website voll umfänglich nutzen können. Durch die Nutzung dieser Website erklären Sie
                sich mit der Bearbeitung der über Sie erhobenen Daten durch Google in der zuvor beschriebenen Art und
                Weise und zu dem zuvor benannten Zweck einverstanden. Weitere Informationen zur&nbsp;Datennutzung durch
                Google bei Ihrer Nutzung&nbsp;dieser Webseite <a href="http://www.google.de/policies/privacy/partners/"
                                                                 class="external" rel="nofollow">erhalten Sie hier</a>.
            </p>
            <h3>Auskunftsrecht</h3>
            <p>Sie haben jederzeit das Recht auf Auskunft über die bezüglich Ihrer Person gespeicherten Daten, deren
                Herkunft und Empfänger sowie den Zweck der Speicherung</p>
        </div>
    </div>
@endsection
