<div class="cover-full cover">
    <div class="cover-inner">
        <div class="row">
            <div class="col-md-2 col-md-offset-1 col-xs-offset-2 col-xs-2 text-center cover-inner-facts-icon">
                <img src="/image/svg/CrocLinks_Icons_Kreise_1.svg">
            </div>
            <div class="col-md-4 col-xs-6 text-left cover-inner-facts-title">
                SHORT IT
            </div>
            <div class="col-md-5 hidden-xs cover-inner-facts-desc">
                ... @lang('frontpage.shortyourlinks')
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 col-md-offset-1 col-xs-offset-2 col-xs-2 text-center cover-inner-facts-icon">
                <img src="/image/svg/CrocLinks_Icons_Kreise_2.svg">
            </div>
            <div class="col-md-4 col-xs-6 text-left cover-inner-facts-title">
                TRACK IT
            </div>
            <div class="col-md-5 hidden-xs cover-inner-facts-desc">
                ... @lang('frontpage.trackyourlinks')
            </div>
        </div>

        <div class="row">
            <div class="col-md-2 col-md-offset-1 col-xs-offset-2 col-xs-2 text-center cover-inner-facts-icon">
                <img src="/image/svg/CrocLinks_Icons_Kreise_3.svg">
            </div>
            <div class="col-md-4 col-xs-6 text-left cover-inner-facts-title">
                PROFIT
            </div>
            <div class="col-md-5 hidden-xs cover-inner-facts-desc">
                ... @lang('frontpage.profitfromlinks')
            </div>
        </div>
        @include('/components/createlink')
    </div>
    <div class="cover-rocket"></div>
    <div class="cover-footer"></div>
    <div class="cover-footer-loop">
        <div class="cover-footer-loop-left"></div>
        <div class="cover-footer-loop-right"></div>
    </div>
    <div class="cover-arrow">
        <i class="fa fa-angle-down" aria-hidden="true"></i>
    </div>
</div>