<div class="container">
    <div class="row frontpage-fact">
        <div class="col-md-2 col-md-offset-2 col-xs-4 col-xs-offset-4">
            <img src="/image/svg/CrocLinks_Icons_Kreise_1.svg" class="img-responsive">
        </div>

        <div class="col-md-6 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <h2>@lang('frontpage.short')</h2>
            <p>@lang('frontpage.short_text').</p>
        </div>
    </div>

    <div class="row frontpage-fact">
        <div class="col-md-2 pull-right"></div>
        <div class="col-md-2 col-md-offset-0 text-center col-xs-4 col-xs-offset-0 pull-right hidden-xs">
            <img src="/image/svg/CrocLinks_Icons_Kreise_2.svg" class="img-responsive">
        </div>

        <div class="col-md-2 col-md-offset-0 text-center col-xs-4 col-xs-offset-4 visible-xs">
            <img src="/image/svg/CrocLinks_Icons_Kreise_2.svg" class="img-responsive">
        </div>


        <div class="col-md-6 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <h2>@lang('frontpage.auswerten')</h2>
            <p>@lang('frontpage.auswerten_text')</p>
        </div>
    </div>

    <div class="row frontpage-fact">
        <div class="col-md-2 col-md-offset-2 col-xs-4 col-xs-offset-4">
            <img src="/image/svg/CrocLinks_Icons_Kreise_3.svg" class="img-responsive">
        </div>

        <div class="col-md-6 col-md-offset-1 col-xs-12 col-xs-offset-0">
            <h2>@lang('frontpage.profit')</h2>
            <p>@lang('frontpage.profit_text')</p>
        </div>
    </div>
</div>