<div class="frontpage-yourlinks">
    <div class="container">
        <h2 class="text-center">WIE PROFITIEREN SIE VON IHREN LINKS?</h2>
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default profit-panel">
                <div class="panel-heading">@lang('frontpage.profit_title')</div>
                <div class="panel-body">
                    <p>@lang('frontpage.profit_desc')</p>
                    <div class="profit-entry">
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                        <h2>Youtube</h2>
                        <p>Subscribe, Like Video, Watch Video</p>
                    </div>
                    <div class="profit-entry">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                        <h2>Facebook</h2>
                        <p>Like Site, Like Post, Share Post, Comment Post</p>
                    </div>
                    <div class="profit-entry">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                        <h2>Twitter</h2>
                        <p>Follow, Retweet</p>
                    </div>
                    <p>@lang('frontpage.profit_subdesc')</p>
                </div>
            </div>
        </div>
    </div>
</div>