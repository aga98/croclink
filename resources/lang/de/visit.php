<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'remove' => 'Löschen',
    'another' => 'weitere',
    'pasteurl' => 'Url hier einfügen',
    'facebookpage' => 'Facebook Seite besuchen',
    'facebookpost' => 'Facebook Post besuchen',
    'instagrampost' => 'Instagram Post besuchen',
    'instagrampage' => 'Instagram Seite besuchen',
    'twitterpost' => 'Twitter Post besuchen',
    'twitterpage' => 'Twitter Seite besuchen',
    'youtubevideo' => 'Youtube Video ansehen',
    'page' => 'Seite ansehen',
    'post' => 'Post ansehen',
    'video' => 'Video ansehen',
    'website' => 'Website',
];
