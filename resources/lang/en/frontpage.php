<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'shortyourlinks' => 'kürzen Sie Ihre Links',
    'trackyourlinks' => 'werten Sie diese aus',
    'profitfromlinks' => ' profitieren Sie von Interaktionen Ihrer User',

    'short' => 'Links kürzen',
    'auswerten' => 'Links auswerten',
    'profit' => 'Profitieren',

    'short_text' => 'Sie wollen einen Link teilen, dieser ist aber Ihnen oder der Plattform zu lang? Kürzen Sie Ihn oder vergeben Sie Ihren eigenen Link',
    'auswerten_text' => 'Nach der Erstellung Ihres Croclinks bieten wir Ihnen die möglichkeit, nachzuvollziehen woher Ihre Klicks kommen. Mit dem Croc-Hud haben Sie eine übersichtliche Zusammenfassung aller Links mit aktuelen Auswertungen.',
    'profit_text' => 'Sie erwarten eine Gegenleistung für die Bereitstellung Ihres Links? Fordern Sie Ihre Nutzer auf, Interakionen auf ausgewählten Plattformen durchzuführen, um weitergeleitet zu werden.',

    'pastelink' => 'Fügen Sie hier Ihren Link ein',

    'profit_title' => 'Aktivieren sie die aufgabenfunktion für ihre user',
    'profit_desc' => 'Ein aktueller Auszug unserer Aufgaben. Nachdem diese Interaktionen erfüllt wurden, wird der user auf deon Ihnen angegebenen Link weitergeleitet:',
    'profit_subdesc' => 'Zusätzlich gibt es noch die Auswahl von Wartezeit und Linkzusendung per E-Mail',

];

