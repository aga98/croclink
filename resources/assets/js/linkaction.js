
$('.js-linkaction-visit').click(function(){
    $(this).find('.action-item-status').removeClass('unchecked').addClass('loading').delay(10000).queue(function(){
        $(this).removeClass("loading").addClass('checked').dequeue();
        var link = $('.js-tolink-btn').attr( "link" );
        console.log(link);
        $('.js-tolink-btn').attr("href", link).dequeue();
        $('.js-tolink-btn').removeAttr('disabled').dequeue();
    });
});